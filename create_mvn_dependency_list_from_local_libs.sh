#!/bin/bash
#This script helps to create a dependency list for maven based on the libraries in ./lib
#So you can copy the lib folder of OmegaT itself here, and the OmegaT.jar, anytime, and have
#a complete list with correct version numbers quickly.

function echo_dependency() {
  local _lib="$1";
  
  local _version=$(echo $lib | grep -Po "(?<=-)[0-9]+.*(?=.jar)")
  local _name=$(echo $lib | grep -Po "(?<=lib/).*?(?=-[0-9]+\..*)")
 
  echo "        <dependency>
            <groupId>libfordebugging</groupId>
            <artifactId>${_name}</artifactId>
            <version>${_version}</version>
            <scope>system</scope>
            <systemPath>\${basedir}/${_lib}</systemPath>
        </dependency>"
}



for lib in $(find lib -name "*.jar"); do

  echo_dependency "$lib"
done; 
   
