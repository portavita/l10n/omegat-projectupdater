# Description
This is a plugin for OmegaT >=5.4 that allows someone to modify 
the source directory locally and commit the files in the remote repository (team project). 

It will add a menu option to do so, scan the changes, report them and ask confirmation to commit them.
This way, no git knowledge/usage is needed to update the sources of the translation project in git.

# build
This plugin is built using maven:

    mvn package
NB: you need to have OmegaT.jar in the /lib directory. And for running that OmegaT with the plugin enabled, you also need all OmegaT libraries under /lib in this /lib dir.

# Install
Copy the created `.jar` file to the plugin directory of OmegaT 

# Usage

For the team project where you want to update sources or TM files: 
make sure you don't have the project open in OmegaT.

Then update the files under `/source` or `/tm` in the project folder root (NB: not under the `.repositories` folder!)

After starting OmegaT, and before loading any project:
under the project menu, click the option `Sync project files to repository`.

The plugin asks you to select a project. Select your project.

Plugin will scan differences under `/source` and ask your confirmation.
Plugin will scan differences under `/tm` and ask your confirmation.

When agreeing, the deletes/modifications/additions will be committed to remote repos.

# Disclaimer
The plugin was developed and tested for a standard team project mapping. 
It probably works for more exotic mappings as well, but use it at your own risk.

Language is English, there are currently no localizations available.

