package eu.portavita.projectupdater;

import org.omegat.core.Core;
import org.omegat.core.CoreEvents;
import org.omegat.core.data.ProjectProperties;
import org.omegat.core.events.IApplicationEventListener;
import org.omegat.gui.main.IMainWindow;
import org.omegat.util.Log;
import org.omegat.util.OStrings;
import org.omegat.util.ProjectFileStorage;
import org.omegat.util.StringUtil;
import org.omegat.util.gui.OmegaTFileChooser;
import org.omegat.util.gui.OpenProjectFileChooser;
import org.omegat.util.gui.UIThreadsUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProjectUpdater {

    private final JMenuItem updateProjectMenuItem;

    protected ActionListener menuItemActionListener = e -> {
        UIThreadsUtil.mustBeSwingThread();

        if (Core.getProject().isProjectLoaded()) {
            return;
        }

        new ProjectUpdaterSwingWorker<Object, Void>() {
        }.execute();
    };

    public ProjectUpdater() {
        updateProjectMenuItem = new JMenuItem("Sync project files to repository");
        updateProjectMenuItem.addActionListener(menuItemActionListener);

        CoreEvents.registerApplicationEventListener(new IApplicationEventListener() {
            public void onApplicationStartup() {
                Core.getMainWindow().getMainMenu().getProjectMenu().add(updateProjectMenuItem);
            }

            /**
             * Called on application shutdown.
             * 
             * CAN BE EXECUTED IN ANY THREAD !
             */
            public void onApplicationShutdown(){}
        });

        CoreEvents.registerProjectChangeListener(eventType -> {
            switch (eventType) {
            case CREATE:
            case LOAD:
                updateProjectMenuItem.setEnabled(false);
                break;
            case CLOSE:
                updateProjectMenuItem.setEnabled(true);
                break;
            }
        });
    }

    public static void displayError(final Throwable ex, final String errorMsg, final Object... params) {
        UIThreadsUtil.executeInSwingThread(() -> {
            String msg;
            if (params != null) {
                msg = StringUtil.format(errorMsg, params);
            } else {
                msg = errorMsg;
            }

            Core.getMainWindow().showStatusMessageRB("TF_ERROR");
            String fulltext = msg;
            if (ex != null) {
                fulltext += "\n" + ex.toString();
            }
            JOptionPane.showMessageDialog(Core.getMainWindow().getApplicationFrame(), fulltext, OStrings.getString("TF_ERROR"),
                    JOptionPane.ERROR_MESSAGE);
        });
    }

    //
    public static void logError(Throwable e, String txt, Object... parameters) {
        Logger LOGGER = Logger.getLogger("global");
        String msg;
        if (parameters != null) {
            msg = StringUtil.format(txt, parameters);
        } else {
            msg = txt;
        }

        LOGGER.log(Level.SEVERE, msg, e);
    }


    static class ProjectUpdaterSwingWorker<Object, Void> extends SwingWorker<Object, Void> {

        Cursor oldCursor = Core.getMainWindow().getCursor();

        @Override
        protected Object doInBackground() throws Exception {
            final File projectRootFolder;
            // select existing project file - open it
            OmegaTFileChooser pfc = new OpenProjectFileChooser();
            if (OmegaTFileChooser.APPROVE_OPTION != pfc.showOpenDialog(Core.getMainWindow()
                    .getApplicationFrame())) {
                return null;
            }
            projectRootFolder = pfc.getSelectedFile();

            IMainWindow mainWindow = Core.getMainWindow();
            Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
            mainWindow.setCursor(hourglassCursor);

            // check if project okay
            ProjectProperties props;
            try {
                props = ProjectFileStorage.loadProjectProperties(projectRootFolder);
            } catch (Exception ex) {
                Log.logErrorRB(ex, "PP_ERROR_UNABLE_TO_READ_PROJECT_FILE");
                Core.getMainWindow().displayErrorRB(ex, "PP_ERROR_UNABLE_TO_READ_PROJECT_FILE");
                return null;
            }

            RemoteRepositoryManager remoteRepositoryProvider;
            if (props.getRepositories() != null) {
                remoteRepositoryProvider = new RemoteRepositoryManager(props.getProjectRootDir(),
                        props.getRepositories(),
                        props);
            } else {
                Core.getMainWindow().showMessageDialog("Project is not a team project."); //"(i) blah"
                mainWindow.setCursor(oldCursor);
                return null;
            }

            remoteRepositoryProvider.commitProjectFilesToRepos();
            Core.getMainWindow().showMessageDialog( "Successfully updated remote repositories.");
            return null;
        }

        @Override
        protected void done() {
            Core.getMainWindow().setCursor(oldCursor);
            try {
                get(); //get the result status of the doInBackground, and throws ExecutionException when an Exception was thrown.
            } catch (ExecutionException e) {
                ProjectUpdater.logError(e.getCause(), "Could not update project files");
                ProjectUpdater.displayError(e.getCause(), "Could not update project files"); //"(X) blah"
            } catch (InterruptedException ex) {
                ProjectUpdater.logError(ex, "Could not update project files");
                ProjectUpdater.displayError(ex, "Could not update project files");
            }
        }


    }
}
