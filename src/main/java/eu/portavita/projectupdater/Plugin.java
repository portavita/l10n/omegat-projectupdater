package eu.portavita.projectupdater;

import org.omegat.core.Core;
import org.omegat.util.OStrings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Plugin {

	private static final String versionTooLow = "ProjectUpdater plugin requires OmegaT 5.4.0 or higher. Your current version is "+OStrings.VERSION;
	private static final String versionTooHigh = "ProjectUpdater plugin requires an older OmegaT version than you currently use. Your current version is "+OStrings.VERSION;

	public static void loadPlugins() {

		try {
			Class<?> clazz = Class.forName("org.omegat.util.VersionChecker");
			Method compareVersions = clazz.getMethod("compareVersions", String.class, String.class, String.class, String.class);
			if ((int)compareVersions.invoke(clazz, OStrings.VERSION, OStrings.UPDATE, "5.4.0", "0") < 0) {
				Core.pluginLoadingError(versionTooLow);
				return;
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			Core.pluginLoadingError(versionTooHigh);
			return;
		} catch (ClassNotFoundException e) {
			Core.pluginLoadingError(versionTooLow);
			return;
		}

		try {
			Class<?> clazz = Class.forName("org.omegat.core.team2.IRemoteRepository2");
			clazz.getMethod("addForDeletion", String.class);
		} catch (NoSuchMethodException e) {
			Core.pluginLoadingError("ProjectUpdater plugin requires a newer Nightly build of OmegaT 5.4.0 (created after 17 sept 2020), or a lower version of OmegaT than you currently use.");
			return;
		} catch (ClassNotFoundException e) {
			Core.pluginLoadingError(versionTooHigh);
			return;
		}

		try {
			new ProjectUpdater();
		} catch (Exception e) {
			Core.pluginLoadingError(e.getLocalizedMessage());
		}
	}
	public static void unloadPlugins() {

	}
}
