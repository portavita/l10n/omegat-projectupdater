package eu.portavita.projectupdater;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.omegat.core.Core;
import org.omegat.core.data.ProjectProperties;
import org.omegat.core.team2.IRemoteRepository2;
import org.omegat.core.team2.IRemoteRepository2.NetworkException;
import org.omegat.core.team2.ProjectTeamSettings;
import org.omegat.core.team2.RemoteRepositoryFactory;
import org.omegat.util.FileUtil;
import org.omegat.util.Log;
import org.omegat.util.StringUtil;

import gen.core.project.RepositoryDefinition;
import gen.core.project.RepositoryMapping;

import javax.swing.*;

import static org.omegat.core.team2.RemoteRepositoryProvider.REPO_SUBDIR;

/**
 * Class for process some repository commands. Based on OmegaTs RemoteRepositoryProvider class.
 *
 * Path, local path, repository path can be directory or one file only. Directory should be declared like
 * 'source/', file should be declared like 'source/text.po'.
 *
 * @author Alex Buloichik (alex73mail@gmail.com)
 * @author Martin Fleurke
 */
public class RemoteRepositoryManager {
	final File projectRoot;
	final ProjectTeamSettings teamSettings;
	final List<RepositoryDefinition> repositoriesDefinitions;
	final List<IRemoteRepository2> repositories = new ArrayList<>();
	final ProjectProperties props;

	public RemoteRepositoryManager(File projectRoot, List<RepositoryDefinition> repositoriesDefinitions, ProjectProperties props)
			throws Exception {
		this.projectRoot = projectRoot;
		this.props = props;
		teamSettings = new ProjectTeamSettings(new File(projectRoot, REPO_SUBDIR));
		this.repositoriesDefinitions = repositoriesDefinitions;

		checkDefinitions();
		initializeRepositories();
	}

	/**
	 * Check repository definitions in the project. TODO: define messages for user
	 */
	protected void checkDefinitions() {
		Set<String> dirs = new TreeSet<>();
		for (RepositoryDefinition r : repositoriesDefinitions) {
			if (StringUtil.isEmpty(r.getUrl())) {
				throw new RuntimeException("There is no repository url");
			}
			if (!dirs.add(getRepositoryDir(r).getAbsolutePath())) {
				throw new RuntimeException("Duplicate repository URL");
			}
			for (RepositoryMapping m : r.getMapping()) {
				if (m.getLocal() == null || m.getRepository() == null) {
					throw new RuntimeException("Invalid mapping: local and/or remote is not set.");
				}
			}
		}
	}

	/**
	 * Initialize repositories instances.
	 */
	protected void initializeRepositories() throws Exception {
		for (RepositoryDefinition r : repositoriesDefinitions) {
			IRemoteRepository2 repo = RemoteRepositoryFactory.create(r.getType());
			repo.init(r, getRepositoryDir(r), teamSettings);
			repositories.add(repo);
		}
	}

	/**
	 * Find mappings for specified path.
	 */
	protected List<RemoteRepositoryManager.Mapping> getMappings(String path, String... forceExcludes) {
		List<RemoteRepositoryManager.Mapping> result = new ArrayList<>();
		for (int i = 0; i < repositoriesDefinitions.size(); i++) {
			RepositoryDefinition rd = repositoriesDefinitions.get(i);
			for (RepositoryMapping repoMapping : rd.getMapping()) {
				RemoteRepositoryManager.Mapping m = new RemoteRepositoryManager.Mapping(path, repositories.get(i), rd, repoMapping, forceExcludes);
				if (m.matches()) {
					result.add(m);
				}
			}
		}
		return result;
	}

	public void commitProjectFilesToRepos() throws Exception {
		Core.getMainWindow().showStatusMessageRB("TEAM_SYNCHRONIZE");
		updateRepos();
		Core.getMainWindow().showStatusMessageRB("TF_COMMIT_START");
		deleteSourceAndTMFilesInReposWhenNotInProjectAnymoreAndCopyFilesAndCommit();
		Core.getMainWindow().showStatusMessageRB("TF_COMMIT_DONE");
	}

	private void deleteSourceAndTMFilesInReposWhenNotInProjectAnymoreAndCopyFilesAndCommit() throws Exception {
		String[] myForceExcludes =
				new String[]{
						'/' + RemoteRepositoryProvider.REPO_SUBDIR,
						'/' + RemoteRepositoryProvider.REPO_GIT_SUBDIR, '/' + RemoteRepositoryProvider.REPO_SVN_SUBDIR,
						'/' + OConsts.FILE_PROJECT,
						'/' + props.getProjectInternalRelative() + OConsts.STATUS_EXTENSION,
						'/' + props.getWritableGlossaryFile().getUnderRoot(),
						'/' + props.getTargetDir().getUnderRoot()};
		if (props.getSourceDir().isUnderRoot()) {

			String sourceDirUnderRoot = props.getSourceDir().getUnderRoot();

			for (RemoteRepositoryManager.Mapping m : getMappings(sourceDirUnderRoot, myForceExcludes)) {
				m.propagateDeleteAndCopyFromProjectToRepo();
			}
			commitFiles(sourceDirUnderRoot, "Commit source files");
		}
		if (props.getTmDir().isUnderRoot()) {
			String tmDirUnderRoot = props.getTmDir().getUnderRoot();
			for (RemoteRepositoryManager.Mapping m : getMappings(tmDirUnderRoot, myForceExcludes)) {
				m.propagateDeleteAndCopyFromProjectToRepo();
			}
			commitFiles(tmDirUnderRoot, "Commit TM files");
		}
	}

	private void updateRepos() throws Exception {
		List<Exception> errors = new ArrayList<>();
		for (IRemoteRepository2 r : repositories) {
			try {
				r.switchToVersion(null);
			} catch (NetworkException e) {
				throw e;
			} catch (Exception e) {
				errors.add(e);
				Log.logErrorRB("TEAM_UPDATE_REPO_ERROR", e.getMessage());
			}
		}
		if (!errors.isEmpty()) {
			throw errors.get(0);
		}
	}

	/**
	 * Commit set of files without rebase - just local version.
	 */
	public void commitFiles(String path, String commentText) throws Exception {
		Map<String, IRemoteRepository2> repos = new TreeMap<>();
		// collect repositories one for one mapping
		for (RemoteRepositoryManager.Mapping m : getMappings(path)) {
			repos.put(m.repoDefinition.getUrl(), m.repo);
		}
		// commit only unique repositories
		for (IRemoteRepository2 repo : repos.values()) {
			repo.commit(null, commentText);
		}
	}

	protected void addForCommit(IRemoteRepository2 repo, String path) throws Exception {
		repo.addForCommit(path);
	}

	protected void addForDelete(IRemoteRepository2 repo, String path) throws Exception {
		repo.addForDeletion(path);
	}

	protected File getRepositoryDir(RepositoryDefinition repo) {
		String path = repo.getUrl().replaceAll("[^A-Za-z0-9.]", "_").replaceAll("__+", "_");
		return new File(new File(projectRoot, REPO_SUBDIR), path);
	}

	static String withLeadingSlash(String s) {
		return s.startsWith("/") ? s : "/" + s;
	}

	static String withTrailingSlash(String s) {
		return s.endsWith("/") ? s : s + "/";
	}

	static String withoutLeadingSlash(String s) {
		return s.startsWith("/") ? s.substring(1) : s;
	}

	static String withoutTrailingSlash(String s) {
		return s.endsWith("/") ? s.substring(0, s.length() - 1) : s;
	}

	static String withSlashes(String s) {
		return withTrailingSlash(withLeadingSlash(s));
	}

	static String withoutSlashes(String s) {
		return withoutTrailingSlash(withoutLeadingSlash(s));
	}

	/**
	 * Class for mapping by specified local path.
	 */
	class Mapping {
		final String filterPrefix;
		final IRemoteRepository2 repo;
		final RepositoryDefinition repoDefinition;
		final RepositoryMapping repoMapping;
		final List<String> forceExcludes;

		Mapping(String path, IRemoteRepository2 repo, RepositoryDefinition repoDefinition,
		        RepositoryMapping repoMapping, String... forceExcludes) {
			this.repo = repo;
			this.repoDefinition = repoDefinition;
			this.repoMapping = repoMapping;
			this.forceExcludes = new ArrayList<>();
			/*
			 * Find common part - it should be one of path or local. If path and local have only common begin,
			 * they will not be mapped. I.e. path=source/ and local=source/one - it's okay, path=source/one/
			 * and local=source/ - also okay, but path=source/one/ and local=source/two - wrong.
			 */
			path = withSlashes(path);
			String local = withSlashes(repoMapping.getLocal());
			if (path.equals("/")) {
				// root(full project path) mapping
				filterPrefix = "/";
				this.forceExcludes.addAll(Arrays.asList(forceExcludes));
			} else if (local.equals(path)) {
				// path equals mapping (path="source/" for "source/"=>"...")
				filterPrefix = "/";
				this.forceExcludes.addAll(getTruncatedExclusions(local, forceExcludes));
			} else if (local.startsWith(path)) {
				// path shorter than local and is directory (path="source/" for "source/first/..."=>"...")
				filterPrefix = "/";
				this.forceExcludes.addAll(getTruncatedExclusions(local, forceExcludes));
			} else if (path.startsWith(local)) {
				// local is shorter than path and is directory (path="omegat/project_save" for
				// "omegat/"=>"...")
				filterPrefix = withSlashes(path.substring(local.length()));
				this.forceExcludes.addAll(Arrays.asList(forceExcludes));
			} else if (local.equals("/")) {
				// root(full project path) mapping (""=>"...")
				filterPrefix = path;
				this.forceExcludes.addAll(Arrays.asList(forceExcludes));
			} else {
				// otherwise path doesn't correspond with repoMapping
				filterPrefix = null;
			}
		}

		List<String> getTruncatedExclusions(String prefix, String... excludes) {
			String normalizedPrefix = withSlashes(prefix);
			return Stream.of(excludes).map(RemoteRepositoryManager::withLeadingSlash)
					.filter(e -> e.startsWith(normalizedPrefix))
					.map(e -> withLeadingSlash(e.substring(normalizedPrefix.length())))
					.collect(Collectors.toList());
		}

		/**
		 * Is path matched with mapping ?
		 */
		public boolean matches() {
			return filterPrefix != null;
		}

		public void propagateDeleteAndCopyFromProjectToRepo() throws Exception {
			if (!matches()) {
				throw new RuntimeException("Path doesn't match with mapping");
			}
			// Remove leading slashes on child args to avoid doing `new
			// File("foo", "/")` which treats the "/" as an actual child element
			// name and prevents proper slash normalization later on.
			File from = new File(projectRoot, withoutLeadingSlash(repoMapping.getLocal()));
			File to = new File(getRepositoryDir(repoDefinition), withoutLeadingSlash(repoMapping.getRepository()));
			if (from.isDirectory()) {
				// directory mapping
				List<String> excludes = new ArrayList<>(repoMapping.getExcludes());
				excludes.addAll(forceExcludes);
				deleteAndCopyToRepo(from, to, filterPrefix, repoMapping.getIncludes(), excludes, repoMapping.getRepository());
			} else {
				// file mapping
				throw new RuntimeException(
							"Cannot use file mapping for "+filterPrefix);
			}
		}

		protected void deleteAndCopyToRepo(File from, File to, String prefix, List<String> includes,
		                                           List<String> excludes, String repoMappingRepo) throws Exception {
			prefix = withSlashes(prefix);

			List<String> relativeFromFiles = FileUtil.buildRelativeFilesList(from, includes, excludes);
			List<String> relativeToDeleteFiles = FileUtil.buildRelativeFilesList(to, includes, excludes);
			relativeToDeleteFiles.removeAll(relativeFromFiles);

			List<String> toDelete = new ArrayList<>();

			for (String rf : relativeToDeleteFiles) {
				rf = withSlashes(rf);
				if (prefix.isEmpty() || prefix.equals("/") || rf.startsWith(prefix)) {
					toDelete.add(rf);
				}
			}

			List<String> toCopy = new ArrayList<>();
			List<String> toAdd = new ArrayList<>();

			for (String rf : relativeFromFiles) {
				rf = withSlashes(rf);
				if (rf.startsWith("/.repositories/")) {
					continue; // list from root - shouldn't travel to .repositories/
				}
				if (prefix.isEmpty() || prefix.equals("/") || rf.startsWith(prefix)) {
					File fromFile = new File(from, rf);
					File toFile = new File(to, rf);
					if (toFile.exists()) {
						if (FileUtils.contentEquals(fromFile, toFile)) {
							continue;
						}
						toCopy.add(rf);
					} else {
						toAdd.add(rf);
					}
				}
			}
			if (toAdd.size() > 0 || toCopy.size() > 0 || toDelete.size() > 0) {
				String message = "For {0} folder in repository {1}:\n" +
						"to delete: {2}, to modify: {3}, to add: {4}\n" +
						"Commit these changes?";
				message = StringUtil.format(message, prefix, this.repoDefinition.getUrl(), toDelete.size(), toCopy.size(), toAdd.size());
				int choice = JOptionPane.showConfirmDialog(Core.getMainWindow().getApplicationFrame(), message, "Continue?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (choice == JOptionPane.YES_OPTION) {
					toCopy.addAll(toAdd);
					for (String rf : toCopy) {
						File fromFile = new File(from, rf);
						File toFile = new File(to, rf);
						copyFile(fromFile, toFile, null);
						addForCommit(repo, withoutSlashes(withoutSlashes(repoMappingRepo)+"/"+withoutSlashes(rf)));
					}
					for (String rf :toDelete) {
						File deleteFile = new File(to, rf);
						if (!deleteFile.delete()) {
							throw new IOException("Could not delete "+deleteFile.getAbsolutePath());
						}
						addForDelete(repo, withoutSlashes(withoutSlashes(repoMappingRepo)+"/"+withoutSlashes(rf)));
					}
				}
			} else {
				String message = StringUtil.format("For {0} folder in {1}:\nNo changes.", prefix, this.repoDefinition.getUrl());
				Core.getMainWindow().showMessageDialog( message);
			}
		}

		public String getVersion() throws Exception {
			return repo.getFileVersion(new File(repoMapping.getRepository(), filterPrefix).getPath());
		}

		private void copyFile(File from, File to, String eolConversionCharset) throws IOException {
			if (eolConversionCharset != null) {
				// charset defined - text file for EOL conversion
				FileUtil.copyFileWithEolConversion(from, to, Charset.forName(eolConversionCharset));
			} else {
				// charset not defined - binary file
				FileUtils.copyFile(from, to);
			}
		}
	}
}
