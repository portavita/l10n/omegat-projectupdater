v1.1.1 (2020-02-25)
- fix for mappings that map to subfolder in repo

v1.1.0 (2020-09-18)
- version for OmegaT 5.4
- maven based
- first public release
